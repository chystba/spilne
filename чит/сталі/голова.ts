// export const букваСька = /[\u0400-\u04FF]/;
export const букваСька = /[А-ЩЬЮЯҐЄІЇа-щьюяґєії]/;
export const словоСька = new RegExp(
  `${букваСька.source}((${букваСька.source}|[-’ʼ\`'])*(${букваСька.source}|-))?\\.?`,
);
export const словоМногоСька = new RegExp(
  словоСька.source,
  словоСька.flags + 'g',
);

export const тількиСловоСька = new RegExp(`^${словоСька.source}$`);
export const тількиПередчепСька = new RegExp(`^${словоСька.source}(-|¬)$`);

export const небукваСька = /[^А-ЩЬЮЯҐЄІЇа-щьюяґєії]/;
export const небукваМногаСька = new RegExp(
  небукваСька.source,
  небукваСька.flags + 'g',
);
export const великаБукваСька = /[А-ЩЬЮЯҐЄІЇ]/;
export const малаБукваСька = /[а-щьюяґєії]/;

export const пропускСька = /\s+/;
export const пропускМногоСька = new RegExp(
  пропускСька.source,
  пропускСька.flags + 'g',
);

export const ГОЛОСНІ_ЗВУКИ = 'аоуиіе';
export const ГОЛОСНІ_БУКВИ = 'аоуиіеяюїє';
export const СКЛАДЕНІ_ГОЛОСНІ_БУКВИ = 'яюїє';
export const ПРИГОЛОСНІ_БУКВИ = 'бвгґджзклмнпрстфхцчшщ';

export const БУКВИ = 'абвгґдеєжзиіїйклмнопрстуфхцчшщьюя';
