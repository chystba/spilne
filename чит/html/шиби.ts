export const шиби = `chystba-pereclad .зірочка {
    position: absolute;
    left: 0%;
    top: -17%;
    color: #0091ff;
    font-weight: bold;
    font-size: 125%;
}

chystba-pereclad chystba-pereclad-obhortka {
}

// chystba-pereclad chystba-pereclad-slovo {
//   position: relative;
//   display: inline-block;
// }

// chystba-pereclad chystba-pereclad-slovo::after {
//   content: "__";
//   font-size: smaller;
//   position: absolute;
//   left: 0px;
//   bottom: 5px;
//   color: transparent;
//   border-bottom-width: 5px;
//   border-bottom-style: solid;
// }

chystba-pereclad-slovo, chystba-pomylka {
  display: inline-block;
}

chystba-pereclad-slovo chystba-pereclad-pervi-znaky, chystba-pomylka::first-letter {
    text-decoration: underline;
    text-decoration-color: #ffa751;
}

chystba-pereclad chystba-pereclad-slovo[vyd-slovozminy="зміна"] chystba-pereclad-pervi-znaky {
  text-decoration-thickness: 3px;
  text-decoration-style: dotted;
}

chystba-pereclad chystba-pereclad-slovo[vyd-slovozminy="відміна"] chystba-pereclad-pervi-znaky {
  text-decoration-thickness: 2px;
  text-decoration-style: wavy;
}

chystba-pomylka::first-letter {
  text-decoration-color: #ff6060;
  text-decoration-thickness: 2px;
}

chystba-pokazchyk {
  box-sizing: content-box;
  position: fixed;
  right: 20px;
  bottom: 7px;
  font-size: 13px;
  line-height: 15px;
  font-weight: bold;
  color: #0000007d;
  text-align: center;
  display: none;
  width: 25px;
  height: 25px;
}
chystba-pokazchyk.vvimknenyi {
  display: block;
}

chystba-pokazchyk:after {
  box-sizing: content-box;
  z-index: 1000;
  position: absolute;
  content: " ";
  left: -3px;
  top: -7.5px;
  display: block;
  width: 22px;
  height: 22px;
  margin: 0px;
  border-radius: 50%;
  border: 4px solid #60cbf1;
  border-color: #60cbf1 transparent #60cbf1 transparent;
  animation: chystba-pokazchyk-ctyahyvach 1.2s linear infinite;
}
@keyframes chystba-pokazchyk-ctyahyvach {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
`;
