import { pick } from 'lodash';
import {
  ПроцідженийРР,
  ВказПерекладуРС,
  РозбірРечення,
  замінаЧастинВСтрічці,
  ВказПерекладуРР,
  ПроцідженийРП,
  РозбірПисьма,
} from '../всказ';

export function рпДаніОбгорнутиПереклади(
  рп: ПроцідженийРП<keyof РозбірПисьма, 'речення' | 'первцевийРР'>,
  писовіДані: ПроцідженийРП<'реченевийРП', 'первцевийРР'>,
  перекладеніДані: ПроцідженийРП<'реченевийРП', 'первцевийРР'>,
  вказиПерекладівРР: ВказПерекладуРР[],
): ПроцідженийРП<keyof РозбірПисьма, 'речення' | 'первцевийРР'> {
  const перекладенийРеченевийРП = рп.реченевийРП.map((рр, вк) => {
    const вказПерекладуРР =
      вказиПерекладівРР.find((пРС) => пРС.вказПервцяРП === вк) ?? null;
    if (!вказПерекладуРР) return рр;

    return ррДаніОбгорнутиПереклади(
      pick(рр, ['речення', 'первцевийРР']),
      pick(писовіДані.реченевийРП[вк], ['первцевийРР']),
      pick(перекладеніДані.реченевийРП[вк], ['первцевийРР']),
      вказПерекладуРР.вказиПерекладівРС,
    );
  });

  let зсувСтрічки = 0;
  const перекладенийПервцевийРП = рп.первцевийРП.map((пп, вк) => {
    const замінник = перекладенийРеченевийРП[вк].речення;

    зсувСтрічки += замінник.length - пп.стрічка.length;
    return {
      стрічка: замінник,
      місцеПоч: пп.місцеПоч + зсувСтрічки,
    };
  });

  const перекладенеHtmlПисьмо = замінаЧастинВСтрічці(
    рп.письмо,
    рп.первцевийРП.map((пп, вк) => {
      return {
        місцеПоч: пп.місцеПоч,
        довжина: пп.стрічка.length,
        замінник: перекладенийРеченевийРП[вк].речення,
      };
    }),
  );

  return {
    письмо: перекладенеHtmlПисьмо,
    первцевийРП: перекладенийПервцевийРП,
    реченевийРП: перекладенийРеченевийРП,
  };
}

function ррДаніОбгорнутиПереклади(
  ррДані: ПроцідженийРР<'речення' | 'первцевийРР'>,
  писовіДані: ПроцідженийРР<'первцевийРР'>,
  перекладеніПисовіДані: ПроцідженийРР<'первцевийРР'>,
  вказиПерекладівРС: ВказПерекладуРС[],
): Pick<РозбірРечення, 'речення' | 'первцевийРР'> {
  let зсувСтрічки = 0;
  const перекладПервцевийHtmlРР: РозбірРечення['первцевийРР'] =
    ррДані.первцевийРР.map((первиннийHtmlПервець, вк) => {
      const первиннийПисовийПервець = писовіДані.первцевийРР[вк];
      const перекладенийПервець = перекладеніПисовіДані.первцевийРР[вк];

      const вказПерекладуРС =
        вказиПерекладівРС.find((пРС) => пРС.вказПервцяРР === вк) ?? null;

      const клстьПервихЗнаків = перекладенийПервець.стрічка.startsWith('д')
        ? 3
        : 2;
      const замінник = !вказПерекладуРС
        ? первиннийHtmlПервець.стрічка
        : '<chystba-pereclad-obhortka>' +
          `<chystba-pereclad-slovo title="${первиннийПисовийПервець.стрічка}" vyd-slovozminy="${вказПерекладуРС.даніПерекладу.видСловозміни}">` +
          `<chystba-pereclad-pervi-znaky>${перекладенийПервець.стрічка.slice(0, клстьПервихЗнаків)}</chystba-pereclad-pervi-znaky>${перекладенийПервець.стрічка.slice(клстьПервихЗнаків)}` +
          `</chystba-pereclad-slovo>` +
          // `<span>${переклад.переклад}</span>` +
          // `<span class="зірочка" title="${переклад.первина}">_</span>` +
          // "</span>" +
          '</chystba-pereclad-obhortka>';

      const місцеПоч = первиннийHtmlПервець.місцеПоч + зсувСтрічки;
      зсувСтрічки += замінник.length - первиннийHtmlПервець.стрічка.length;
      return {
        місцеПоч,
        стрічка: замінник,
      };
    });

  const перекладенеHtmlРечення = замінаЧастинВСтрічці(
    ррДані.речення,
    вказиПерекладівРС.map((пРС) => {
      return {
        місцеПоч: ррДані.первцевийРР[пРС.вказПервцяРР].місцеПоч,
        довжина: ррДані.первцевийРР[пРС.вказПервцяРР].стрічка.length,
        замінник: перекладПервцевийHtmlРР[пРС.вказПервцяРР].стрічка,
      };
    }),
  );

  return {
    речення: перекладенеHtmlРечення,
    первцевийРР: перекладПервцевийHtmlРР,
  };
}

// function вирівняти () {
// TODO: точніше підібарти розмір шрифта
// const первиниHTML = рядокHTML.querySelectorAll('.первина')

// for(var i = 0; i < первиниHTML.length; i++)  {
//     const первинаHTML = первиниHTML[i] as HTMLElement

//     const { width, height } = первинаHTML.getBoundingClientRect()

//     // const перекладHTML = первинаHTML.querySelector('.переклад') as (HTMLElement | null)
//     // if (!перекладHTML) throw new Error('Неочікувана помилка')
//     первинаHTML.style.width = width + 'px'
//     первинаHTML.style.height = height + 'px'
// }

// textfit(document.querySelectorAll('.переклад'))
// }
