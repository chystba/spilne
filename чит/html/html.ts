export * from './шиби';

import {
  ПроцідженийПерекладРП,
  ПроцідженийРП,
  ПроцідженийРР,
  РозбірПисьма,
} from '../види/види';
import { рпДаніОбгорнутиПереклади } from './обгортка-перекладів';

export async function перекластиHtmlРядки(
  рядкиHTML: HTMLElement[],
  перекластиРП: (
    письма: string[],
  ) => Promise<
    (ПроцідженийПерекладРП<
      keyof РозбірПисьма,
      'речення' | 'первцевийРР'
    > | null)[]
  >,
) {
  const розмірПучку = 100;
  const межаРядків = 1000;
  for (let i = 0; i < межаРядків; i += розмірПучку) {
    const пучокРядоківHTML = рядкиHTML.slice(i, i + розмірПучку);
    if (пучокРядоківHTML.length <= 0) continue;

    const пучокПереклади = await перекластиРП(
      пучокРядоківHTML.map((р) =>
        додатковеОчищенняTextContent(р.textContent ?? ''),
      ),
    );
    if (пучокПереклади == null) continue;

    пучокРядоківHTML.forEach((р, вк) => {
      const перекладРП = пучокПереклади[вк];
      // console.log(р.innerHTML, р.textContent, пучокПереклади[вк])

      if (перекладРП) {
        const htmlРП = писовийРПДаніВHtmlРПДані(перекладРП.рп, р.innerHTML);

        const перекладенийHtmlРП = рпДаніОбгорнутиПереклади(
          htmlРП,
          перекладРП.рп,
          перекладРП.перекладенийРП,
          перекладРП.вказиПерекладівРР,
        );
        р.innerHTML = `<chystba-pereclad title="${
          р.textContent ?? ''
        }">${перекладенийHtmlРП.письмо}</chystba-pereclad>`;
        // вирівняти()
      } else {
        р.innerHTML = `<chystba-pomylka>${р.innerHTML}</chystba-pomylka>`;
      }
    });
  }
}

export function писовийРПДаніВHtmlРПДані(
  писовийРП: ПроцідженийРП<keyof РозбірПисьма, 'речення' | 'первцевийРР'>,
  первиннийHtml: string,
): ПроцідженийРП<keyof РозбірПисьма, 'речення' | 'первцевийРР'> {
  const htmlПервцевийРП = стрічкиВHtmlСтрічки(
    писовийРП.первцевийРП,
    первиннийHtml,
  );
  const htmlРеченевийРП = писовийРП.реченевийРП.map((рр, вк) =>
    писовийРРДаніВHtmlРРДані(рр, htmlПервцевийРП[вк].стрічка),
  );

  return {
    письмо: первиннийHtml,
    первцевийРП: htmlПервцевийРП,
    реченевийРП: htmlРеченевийРП,
  };
}

export function писовийРРДаніВHtmlРРДані(
  писовийРР: ПроцідженийРР<'речення' | 'первцевийРР'>,
  первиннийHtml: string,
): ПроцідженийРР<'речення' | 'первцевийРР'> {
  return {
    ...structuredClone(писовийРР),
    речення: первиннийHtml,
    первцевийРР: стрічкиВHtmlСтрічки(писовийРР.первцевийРР, первиннийHtml),
  };
}

let первеньДляДовжиниПервнівHTML: HTMLElement | null = null;
function довжинаПервняHTML(письмо: string) {
  if (!первеньДляДовжиниПервнівHTML) {
    class PerekladDovdjynaPervnivElement extends HTMLElement {}
    window.customElements.define(
      'pereklad-dovdjyna-pervniv',
      PerekladDovdjynaPervnivElement,
    );
    первеньДляДовжиниПервнівHTML = new PerekladDovdjynaPervnivElement();
  }

  if (письмо.startsWith('<') && письмо.endsWith('>')) return 0;
  первеньДляДовжиниПервнівHTML.innerHTML = письмо;
  return (
    додатковеОчищенняTextContent(первеньДляДовжиниПервнівHTML.textContent ?? '')
      .length ?? 0
  );
}

export function стрічкиВHtmlСтрічки(
  стрічки: { стрічка: string; місцеПоч: number }[],
  htmlСтрічка: string,
): { стрічка: string; місцеПоч: number }[] {
  const htmlСтрічки = [];

  const збігиПервніHTML = Array.from(
    htmlСтрічка.matchAll(
      /(<.*?>)|\u00AD|(&(?:[a-z0-9]+|#[0-9]{1,6}|#x[0-9a-fA-F]{1,6});)/gi,
    ),
  );
  // console.log(збігиПервніHTML)
  let пройденіЗбігиВк = 0;
  let зсувЧерезHTML = 0;
  for (const { стрічка, місцеПоч } of стрічки) {
    let довжина = стрічка.length;
    // console.log(переклад.переклад, пройденіЗбігиВк, зсувЧерезHTML)
    for (let вк = пройденіЗбігиВк; вк <= збігиПервніHTML.length; вк++) {
      if (вк === збігиПервніHTML.length) {
        пройденіЗбігиВк = вк;
        break;
      }

      const збігПервня = збігиПервніHTML[вк];
      if (збігПервня.index === undefined) throw new Error('Невідома помилка');

      const зсувПервня =
        збігПервня[0].length - довжинаПервняHTML(збігПервня[0]);

      if (збігПервня.index <= місцеПоч + зсувЧерезHTML) {
        // console.log('before', зсувЧерезHTML, збігПервня[0].length, збігПервня, зсувПервня)
        зсувЧерезHTML += зсувПервня;
      } else if (
        збігПервня.index > місцеПоч + зсувЧерезHTML &&
        збігПервня.index < місцеПоч + зсувЧерезHTML + довжина
      ) {
        // console.log('in', збігПервня)
        довжина += зсувПервня;
      } else {
        пройденіЗбігиВк = вк;
        break;
      }
    }

    htmlСтрічки.push({
      місцеПоч: місцеПоч + зсувЧерезHTML,
      стрічка: htmlСтрічка.slice(
        місцеПоч + зсувЧерезHTML,
        місцеПоч + зсувЧерезHTML + довжина,
      ),
    });

    зсувЧерезHTML += довжина - стрічка.length;
  }

  return htmlСтрічки;
}

function додатковеОчищенняTextContent(стр: string) {
  // видаляє "&shy;"
  return стр.replaceAll(/\u00AD/g, '');
}
