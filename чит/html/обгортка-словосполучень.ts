import {
  ПроцідженийРР,
  РозбірРечення,
  замінаЧастинВСтрічці,
  ПроцідженийРП,
  РозбірПисьма,
  ладиннийРРВНадрядніССВідбит,
  вПнПРСловосполукиПоВказу,
} from '../всказ';

export function рпДаніОбгорнутиСловосполучення(
  рп: ПроцідженийРП<
    keyof РозбірПисьма,
    'речення' | 'первцевийРР' | 'ладиннийРР'
  >,
): ПроцідженийРП<keyof РозбірПисьма, 'речення'> {
  return {
    ...рп,
    реченевийРП: рп.реченевийРП.map((рр) => ррДаніОбгорнутиСловосполучення(рр)),
  };
}

function ррДаніОбгорнутиСловосполучення(
  ррДані: ПроцідженийРР<'речення' | 'первцевийРР' | 'ладиннийРР'>,
): Pick<РозбірРечення, 'речення'> {
  const надрядніССВідбит = ладиннийРРВНадрядніССВідбит(ррДані.ладиннийРР);
  const замінники = ррДані.первцевийРР.map((пр, вк) => {
    const надрядніСС = надрядніССВідбит.get(вк) ?? [];
    const почСС = надрядніСС.filter(
      (сс) => вПнПРСловосполукиПоВказу(сс, 0) === вк,
    );
    const кінСС = надрядніСС.filter(
      (сс) => вПнПРСловосполукиПоВказу(сс, -1) === вк,
    );

    const почHtmlПервні = почСС
      .map(
        (сс) =>
          `<chystba-slovospoluka vyd="${сс.даніСловосполуки.вид}" ryadnist="${сс.даніСловосполуки.рядність}">`,
      )
      .join('');
    const кінHtmlПервні = кінСС.map(() => `</chystba-slovospoluka>`).join('');

    const замінник = почHtmlПервні + пр.стрічка + кінHtmlПервні;

    return замінник;
  });

  const заміненеHtmlРечення = замінаЧастинВСтрічці(
    ррДані.речення,
    замінники.map((замінник, вк) => {
      return {
        місцеПоч: ррДані.первцевийРР[вк].місцеПоч,
        довжина: ррДані.первцевийРР[вк].стрічка.length,
        замінник,
      };
    }),
  );

  return {
    речення: заміненеHtmlРечення,
  };
}
