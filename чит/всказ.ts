export * from './види/види';
export * from './html/html';
export * from './переклад/переклад';
export * from './розбір/розбір';
export * from './загальне/всказ';
export * from './буквостан/голова';
export * from './сталі/голова';
