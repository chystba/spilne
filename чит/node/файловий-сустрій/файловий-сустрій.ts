import { createWriteStream, existsSync, mkdirSync, unlinkSync } from 'fs';
import { dirname, join, resolve } from 'path';
import { homedir, platform } from 'os';
import { Readable } from 'stream';
import { finished } from 'stream/promises';
import { ReadableStream } from 'stream/web';

export function ручитиИснуванняПутіФайлаОБ(путь: string) {
  const путьПапки = dirname(путь);
  ручитиИснуванняПутіПапкиОБ(путьПапки);
}

export function ручитиИснуванняПутіПапкиОБ(путьПапки: string) {
  if (!existsSync(путьПапки)) {
    mkdirSync(путьПапки, { recursive: true });
  }
}

export async function стягнутиВіддаленийСніпВСніп(
  місцевйиПутьФайлу: string,
  віддаленийПутьФайлу: string,
) {
  const stream = createWriteStream(місцевйиПутьФайлу);
  const { body } = await fetch(віддаленийПутьФайлу);
  if (!body) throw new Error('Неможливо стягнути файл');
  return await finished(Readable.fromWeb(body as ReadableStream).pipe(stream));
}

export async function стягнутиВіддаленийСніпВДані(віддаленийПутьФайлу: string) {
  const відп = await fetch(віддаленийПутьФайлу);
  if (!відп.ok) throw new Error('Неможливо стягнути файл');
  return Buffer.from(await відп.arrayBuffer());
}

export function ручитиНеиснуванняФайлуОБ(путь: string) {
  try {
    unlinkSync(путь);
  } catch (err) {
    // An error occurred while deleting the file
    if (
      !(
        err &&
        typeof err === 'object' &&
        'code' in err &&
        err.code === 'ENOENT'
      )
    ) {
      // Some other error
      throw err;
    }
  }
}

export function витягСніпниціПідхову(підпуть: string) {
  const тимчСніпниця = resolve(
    resolve(
      process.env.PIDKHIVNA_SNIPNYTSIA || отрСніпницюПідховуОсновини(),
      підпуть,
    ),
  );
  ручитиИснуванняПутіПапкиОБ(тимчСніпниця);

  return тимчСніпниця;
}

export function отрСніпницюПідховуОсновини() {
  const основина = platform();

  if (основина === 'win32') {
    // Для Windows
    return join(homedir(), 'AppData', 'Local', 'Temp');
  } else if (основина === 'darwin') {
    // Для macOS
    return join(homedir(), 'Library', 'Caches');
  } else {
    // Для Linux
    return join(homedir(), '.cache');
  }
}
