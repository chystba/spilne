import { exec } from 'child_process';

export function виконати(рать: string) {
  const child = exec(рать);

  child.stdout?.on('data', function (data) {
    // eslint-disable-next-line
    console.log(data);
  });
  child.stderr?.on('data', function (data) {
    // eslint-disable-next-line
    console.log(data);
  });
  child.on('close', function (code) {
    // eslint-disable-next-line
    console.log('closing code: ' + code);
  });

  return new Promise(function (resolve, reject) {
    child.addListener('error', reject);
    child.addListener('exit', resolve);
  });
}
