import { KeyvSqlite } from '@keyv/sqlite';
import Keyv from 'keyv';
import СніповийПідхів from './сніповий-підхів';

export function створитиSqliteПідхів(сніпниця: string) {
  return new Keyv({ store: new KeyvSqlite(сніпниця) });
}

export function створитиСніповийПідхів(сніпниця: string) {
  return new Keyv({ store: new СніповийПідхів(сніпниця) });
}
