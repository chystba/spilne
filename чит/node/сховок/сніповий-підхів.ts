// src/index.ts
import EventEmitter from 'events';
import { resolve } from 'node:path';
import { access, mkdir, readFile, rm, writeFile } from 'fs/promises';
import { F_OK } from 'node:constants';
import { defaultSerialize, defaultDeserialize } from '@keyv/serialize';
import { KeyvStoreAdapter, StoredData } from 'keyv';
import { isBinaryFile } from 'isbinaryfile';

type KeyvSqliteOptions = {
  сніпниця: string;
  dialect: string;
  iterationLimit: number;
  serialize: typeof defaultSerialize;
  deserialize: typeof defaultDeserialize<{
    expires: number;
    value: string | Buffer;
  }>;
};

export interface WrappedValue<T = any> {
  value: T;
  expire?: number;
}

export default class СніповийПідхів
  extends EventEmitter
  implements KeyvStoreAdapter
{
  ttlSupport;
  opts: KeyvSqliteOptions;
  constructor(keyvOptions: Partial<KeyvSqliteOptions> | string) {
    super();
    this.ttlSupport = false;

    let options: Partial<KeyvSqliteOptions> = {};
    if (typeof keyvOptions === 'string') {
      options.сніпниця = keyvOptions;
    } else {
      options = {
        ...options,
        ...keyvOptions,
      };
    }
    if (!options.сніпниця) throw new Error('Сніпниця повинна иснувати');
    if (!options.serialize) options.serialize = defaultSerialize;
    if (!options.deserialize) options.deserialize = defaultDeserialize;
    if (!options.dialect) options.dialect = 'сніповий';

    this.opts = options as KeyvSqliteOptions;
  }
  async get<Value>(ключ: string): Promise<StoredData<Value> | undefined> {
    return this.opts.serialize({
      value: await this.розчитити(
        await readFile(resolve(this.opts.сніпниця, ключ)),
      ),
      expires: null,
    }) as Value;
  }
  async getMany<Value>(
    ключі: string[],
  ): Promise<Array<StoredData<Value | undefined>>> {
    return Promise.all(ключі.map((ключ) => this.get<Value>(ключ)));
  }
  async set(ключ: string, знач: string) {
    const путь = resolve(this.opts.сніпниця, ключ);

    await writeFile(путь, this.вчитити(this.opts.deserialize(знач).value));
  }

  async delete(ключ: string) {
    const путь = resolve(this.opts.сніпниця, ключ);
    if (!(await this.чиИснує(путь))) {
      return false;
    } else {
      await rm(путь, { recursive: true });
      return true;
    }
  }
  async deleteMany(ключі: string[]) {
    const висліди = ключі.map((ключ) => this.delete(ключ));
    return висліди.some((в) => в);
  }
  async clear() {
    await rm(this.opts.сніпниця, { recursive: true });
    await mkdir(this.opts.сніпниця);
  }
  //   async *iterator<Value>() {
  //     const назвиСніпницьЗначень = await readdir(this.opts.сніпниця);
  //     const обмеження = this.opts.iterationLimit || 10;
  //     async function* iterate(
  //       зсув: number,
  //       запит: (к: string[]) => Promise<{ ключ: string; знач: Value }[]>,
  //     ): AsyncGenerator<[string, string], void, void> {
  //       const вибірка = назвиСніпницьЗначень.slice(зсув, зсув + обмеження);
  //       const entries = await запит(вибірка);
  //       if (entries.length === 0) {
  //         return;
  //       }
  //       for (const entry of entries) {
  //         зсув += 1;
  //         yield [entry.ключ, entry.знач];
  //       }
  //       yield* iterate(зсув, запит);
  //     }
  //     yield* iterate(0, async (ключі: string[]) => {
  //       const дані = await this.getMany<string>(ключі);
  //       return ключі.map((ключ, вк) => ({ ключ, значСтр: дані[вк].value }));
  //     });
  //   }
  async has(ключ: string) {
    const путь = resolve(this.opts.сніпниця, ключ);
    return await this.чиИснує(путь);
  }
  async disconnect() {
    return;
  }

  private вчитити<Value>(знач: Value): Buffer {
    return Buffer.isBuffer(знач) ? знач : Buffer.from(String(знач));
  }

  private async розчитити(дані: Buffer) {
    if (await isBinaryFile(дані, дані.length)) {
      return дані;
    } else {
      return дані.toString();
    }
  }

  private async чиИснує(путь: string) {
    try {
      await access(путь, F_OK);
      return true;
    } catch (e) {
      return false;
    }
  }
}
