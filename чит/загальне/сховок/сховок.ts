import { createCache, Cache } from 'cache-manager';
import { Keyv } from 'keyv';
import { Пірописець } from '../всказ';

export { Cache } from 'cache-manager';

export function створитиСховок(сховок?: Keyv) {
  return createCache({ stores: [сховок ?? new Keyv()] });
}

export async function сховокРучитиТаВитягти<Д>(
  ключ: string,
  час: number,
  витягДаних: () => Д,
  сховокРядник: Cache,
  пірописець?: Пірописець,
): Promise<Д> {
  const ключСховка = ключ;

  let дані = await сховокРядник.get<Д>(ключСховка);

  if (!дані) {
    пірописець?.info(`Початок витягу даних з сховка під ключом "${ключ}"`);
    дані = await витягДаних();
    пірописець?.info('Кінець витягу даних');

    await сховокРядник.set(ключСховка, дані, час);
  }

  return дані;
}
