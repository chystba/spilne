export function розбірДужок(
  str: string,
  opts?: { brackets?: string | string[]; escape?: string; flat?: boolean },
) {
  // pretend non-string parsed per-se
  if (typeof str !== 'string') return [str];

  let res = [str];

  if (typeof opts === 'string' || Array.isArray(opts)) {
    opts = { brackets: opts };
  } else if (!opts) opts = {};

  const brackets = opts.brackets
    ? Array.isArray(opts.brackets)
      ? opts.brackets
      : [opts.brackets]
    : ['{}', '[]', '()'];

  const escape = opts.escape || '___';

  const flat = !!opts.flat;

  brackets.forEach(function (bracket) {
    // create parenthesis regex
    const pRE = new RegExp(
      [
        '\\',
        bracket[0],
        '[^\\',
        bracket[0],
        '\\',
        bracket[1],
        ']*\\',
        bracket[1],
      ].join(''),
    );

    let ids: number[] = [];

    function replaceToken(token: string) {
      // save token to res
      const refId =
        res.push(token.slice(bracket[0]!.length, -bracket[1]!.length)) - 1;

      ids.push(refId);

      return escape + refId + escape;
    }

    res.forEach(function (str, i) {
      let prevStr;

      // replace paren tokens till there’s none
      let a = 0;
      while (str != prevStr) {
        prevStr = str;
        str = str.replace(pRE, replaceToken);
        if (a++ > 10e3)
          throw Error(
            'References have circular dependency. Please, check them.',
          );
      }

      res[i] = str;
    });

    // wrap found refs to brackets
    ids = ids.reverse();
    res = res.map(function (str) {
      ids.forEach(function (id) {
        str = str.replace(
          new RegExp('(\\' + escape + id + '\\' + escape + ')', 'g'),
          bracket[0] + '$1' + bracket[1],
        );
      });
      return str;
    });
  });

  const re = new RegExp('\\' + escape + '([0-9]+)' + '\\' + escape);

  // transform references to tree
  function nest(str: string, refs: string[]) {
    type РозбірДужок = string | РозбірДужок[];
    const res: РозбірДужок = [];
    let match: RegExpExecArray | null;

    let a = 0;
    while ((match = re.exec(str))) {
      if (a++ > 10e3) throw Error('Circular references in parenthesis');
      res.push(str.slice(0, match.index - 1));

      res.push(nest(refs[Number(match[1])]!, refs));

      str = str.slice(match.index + match[0].length + 1);
    }

    res.push(str);

    return res;
  }

  return flat ? res : nest(res[0]!, res);
}
