export async function однобіжно<T, R>(
  iterable: Iterable<T>,
  fn: (a: T, вк: number) => Promise<R>,
): Promise<R[]> {
  const results = [];

  let вк = 0;
  for (const x of iterable) {
    results.push(await fn(x, вк));
    вк++;
  }

  return results;
}

export async function багатобіжно<T, R>(
  iterable: Iterable<T>,
  fn: (a: T) => Promise<R>,
): Promise<R[]> {
  const results = [];

  for (const x of iterable) {
    results.push(fn(x));
  }

  return Promise.all(results);
}

export async function процідитиОднобіжно<T>(
  iterable: Iterable<T>,
  fn: (a: T) => Promise<boolean>,
): Promise<T[]> {
  const первні = [];

  for (const x of iterable) {
    const вислід = await fn(x);
    if (вислід) первні.push(x);
  }

  return первні;
}

export function ськатиДвійково<П>(
  відхухренийРяд: П[],
  зрівняти: (п: П) => number,
) {
  let m = 0;
  let n = відхухренийРяд.length - 1;
  while (m <= n) {
    const k = (n + m) >> 1;
    const cmp = зрівняти(відхухренийРяд[k]!);
    if (cmp > 0) {
      m = k + 1;
    } else if (cmp < 0) {
      n = k - 1;
    } else {
      return відхухренийРяд[k];
    }
  }

  const вк = ~m;
  return вк === -1 ? null : відхухренийРяд[вк];
}

export function чиНалежить<П>(
  первеньСпиту: any,
  ряд: Array<П> | ReadonlyArray<П>,
): первеньСпиту is П {
  return ряд.includes(первеньСпиту as П);
}

export function увузитиПо<О, К extends string>(
  ряд: О[],
  по: (об: О) => К,
): Map<К, О[]> {
  const map = new Map<К, О[]>();
  ряд.forEach((item) => {
    const key = по(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  });
  return map;
}

export function увузитиТаВідбитиПо<В, О, К extends string>(
  ряд: О[],
  по: (об: О) => [К, В],
): Map<К, В[]> {
  const map = new Map<К, В[]>();
  ряд.forEach((item) => {
    const [key, вислід] = по(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [вислід]);
    } else {
      collection.push(вислід);
    }
  });
  return map;
}

export function ручитиПервеньВРяді<T>(ряд: T[], первень: T): T[] {
  if (ряд.includes(первень)) return [...ряд];
  else return [...ряд, первень];
}

export function ручитиПервніВРяді<T>(ряд: T[], первні: T[]): T[] {
  let новийРяд = [...ряд];

  первні.forEach((п) => {
    новийРяд = ручитиПервеньВРяді(новийРяд, п);
  });

  return новийРяд;
}

export function ручитиВідсутністьПервняВРяді<T>(ряд: T[], первень: T): T[] {
  const вк = ряд.indexOf(первень);
  if (вк > -1) {
    ряд.splice(вк, 1);
    return ряд;
  } else {
    return ряд;
  }
}

export function цігліСтрічки<T>(a: T[], по: (п: T) => string): T[] {
  const seen: Record<string, number> = {};
  const out: T[] = [];
  const len = a.length;
  let j = 0;
  for (let i = 0; i < len; i++) {
    const item = a[i]!;
    const itemValue = по(item);
    if (seen[itemValue] !== 1) {
      seen[itemValue] = 1;
      out[j++] = item;
    }
  }
  return out;
}

export function перемішати<T>(лад: T[]): T[] {
  let поточнийВк = лад.length,
    жеребийВк;

  while (поточнийВк > 0) {
    жеребийВк = Math.floor(Math.random() * поточнийВк);
    поточнийВк--;

    [лад[поточнийВк], лад[жеребийВк]] = [лад[жеребийВк]!, лад[поточнийВк]!];
  }

  return лад;
}

export function знайтиВсіВкази<T>(
  лад: T[],
  ська: (первень: T) => boolean,
): number[] {
  const indexes = [];
  for (let i = 0; i < лад.length; i++) if (ська(лад[i]!)) indexes.push(i);
  return indexes;
}

export function зтяженіВипадкові<T>(
  лад: T[],
  тяжПо: (первень: T) => number,
  кількість: number,
): T[] {
  const випадковіПервні = [];
  const тяжі = лад.map(тяжПо);
  const тяжіГал = тяжі.reduce((тяжіГал, тяж, i) => {
    тяжіГал.push(тяж + (тяжіГал[i - 1] ?? 0));
    return тяжіГал;
  }, [] as number[]);

  for (let _ = 0; _ < кількість; _++) {
    const випадкове = Math.random() * тяжіГал[тяжіГал.length - 1]!;

    const випадковийВк = тяжіГал.findIndex((тяж) => тяж > випадкове);
    if (випадковийВк >= 0) {
      випадковіПервні.push(лад[випадковийВк]!);

      const поточнийТяж = тяжі[випадковийВк]!;
      for (let i = випадковийВк; i < тяжіГал.length; i++) {
        тяжіГал[випадковийВк]! -= поточнийТяж;
      }
    } else {
      throw new Error('Щось пішло не так');
    }
  }

  return випадковіПервні;
}

export function всіМножиниЗєднів<П>(
  ряд: П[],
  сполучник: string,
  найбДовжЗєдня = ряд.length,
): (П | string)[][] {
  if (ряд.length === 0 || найбДовжЗєдня === 1) return [[...ряд]];

  const множиниЗєднів = [];
  for (let вк = 0; вк <= ряд.length - найбДовжЗєдня; вк++) {
    const передПідмножиниЗєднів = всіМножиниЗєднів(
      ряд.slice(0, вк),
      сполучник,
      найбДовжЗєдня - 1,
    );
    const післяПідмножиниЗєднів = всіМножиниЗєднів(
      ряд.slice(вк + найбДовжЗєдня),
      сполучник,
      найбДовжЗєдня,
    );

    for (const передПідмножина of передПідмножиниЗєднів) {
      for (const післяПідмножина of післяПідмножиниЗєднів) {
        множиниЗєднів.push([
          ...передПідмножина,
          ряд.slice(вк, вк + найбДовжЗєдня).join(сполучник),
          ...післяПідмножина,
        ]);
      }
    }
  }

  return [
    ...множиниЗєднів,
    ...всіМножиниЗєднів(ряд, сполучник, найбДовжЗєдня - 1),
  ];
}
