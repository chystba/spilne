export function узвичитиСлово(слово: string) {
  return слово
    .normalize()
    .replaceAll('’', "'")
    .replaceAll('`', "'")
    .replaceAll('ʼ', "'")
    .replace(/\p{Diacritic}/gu, '');
}
