import { getLogger } from 'loglevel';

export function стПірописець(назва: string) {
  const пірописець = getLogger(назва);
  пірописець.setLevel('TRACE');
  return пірописець;
}

export type Пірописець = ReturnType<typeof стПірописець>;
