import { pick } from 'lodash';
import {
  ПнПР,
  ПроцідженийРП,
  ПроцідженийРР,
  ПроцідженийРС,
  РозбірПисьма,
  РозбірРечення,
  РозбірСлова,
  Словосполука,
} from '../види/види';

export function процідитиРП<
  КлючіРП extends keyof РозбірПисьма = keyof РозбірПисьма,
  КлючіРР extends keyof РозбірРечення = keyof РозбірРечення,
  КлючіРС extends keyof РозбірСлова = keyof РозбірСлова,
>(
  рп: РозбірПисьма,
  ключіРП?: КлючіРП[],
  ключіРР?: КлючіРР[],
  ключіРС?: КлючіРС[],
): ПроцідженийРП<КлючіРП, КлючіРР, КлючіРС> {
  const оновленийРП = {
    ...рп,
    реченевийРП: рп.реченевийРП.map((рр) => процідитиРР(рр, ключіРР, ключіРС)),
  };
  return ключіРП ? pick(оновленийРП, ключіРП) : оновленийРП;
}

export function процідитиРР<
  КлючіРР extends keyof РозбірРечення = keyof РозбірРечення,
  КлючіРС extends keyof РозбірСлова = keyof РозбірСлова,
>(
  рр: РозбірРечення,
  ключіРР?: КлючіРР[],
  ключіРС?: КлючіРС[],
): ПроцідженийРР<КлючіРР, КлючіРС> {
  const оновленийРР = {
    ...рр,
    слівнийРР: Object.fromEntries(
      Object.entries(рр.слівнийРР).map(([вк, рс]) => [
        вк,
        процідитиРС(рс, ключіРС),
      ]),
    ),
  };
  return ключіРР ? pick(оновленийРР, ключіРР) : оновленийРР;
}

export function процідитиРС<
  КлючіРС extends keyof РозбірСлова = keyof РозбірСлова,
>(рс: РозбірСлова, ключіРС?: КлючіРС[]): ПроцідженийРС<КлючіРС> {
  return ключіРС ? pick(рс, ключіРС) : рс;
}

export function ладиннийРРВНадрядніССВідбит(
  ладиннийРР: РозбірРечення['ладиннийРР'],
) {
  const надрядніССВідбит = new Map<ПнПР, Словосполука[]>();
  function обробитиСС(сс: Словосполука) {
    for (const част of сс.частини) {
      if (typeof част === 'number') {
        const потНадрядні = надрядніССВідбит.get(част) ?? [];
        надрядніССВідбит.set(част, [...потНадрядні, сс]);
      } else {
        обробитиСС(част);
      }
    }
  }
  ладиннийРР.сс.forEach(обробитиСС);

  return надрядніССВідбит;
}

export function вПнПРСловосполукиПоВказу(сс: Словосполука, вказ: number) {
  const част = сс.частини.at(вказ);
  if (част === undefined) throw new Error('Немає частини на даному вказі');

  if (typeof част === 'number') return част;
  else return вПнПРСловосполукиПоВказу(част, вказ);
}
